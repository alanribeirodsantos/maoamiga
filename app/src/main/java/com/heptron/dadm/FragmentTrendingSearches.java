package com.heptron.dadm;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by AlanR on 22/10/2017.
 */

public class FragmentTrendingSearches extends Fragment {

    int photos[] = {R.drawable.jardinagem_2, R.drawable.alvenaria_1, R.drawable.encanamento_2, R.drawable.pintura_1,
            R.drawable.gas_1, R.drawable.encanamento_1};

    String names[] = {"Isis Teixeira", "José Maria", "Adriano Santos", "Rosa dos Santos", "Raquel Maia", "Carlos Oliveira"};

    String occupations[] = {"Jardinagem", "Alvenaria", "Encanamento", "Pintura", "Instalação de Gás", "Encanamento"};

    String addresses[] = {"Travessa Adalto Lino, Alto São Francisco", "Rua Nossa Senhora Aparecida, Putiú", "Travessa José Capistrano Filho, Curicaca",
                          "Beco 2 Francisco Batista de Queiroz, Alto Boa Vista", "Rua Pascoal Crispino, Centro ", "Rua Raimundo Tavares de Almeida, Jardim dos Monólitos"};

    String phones[] = {"(88) 98165-0008", "(88) 99765-7856", "(88) 99625-1234", "(88) 99964-9860", "(88) 97645-8967", "(88) 98156-9866"};

    int evaluations[] = {5, 4, 5, 3, 4, 4};

    int[][] daysBusy = new int[][]{
            new int[] {18, 20},
            new int[] {15, 18, 19, 22, 27},
            new int[] {17, 20, 21, 23},
            new int[] {18, 19, 29},
            new int[] {15, 27},
            new int[] {19, 20}
    };

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trending_searches, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.trendingRecycler);
        TrendingAdapter adapter = new TrendingAdapter(getActivity(), photos, names, occupations, addresses, phones, evaluations, daysBusy);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        return view;
    }


}

