package com.heptron.dadm;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.Calendar;

public class WorkerProfile extends AppCompatActivity {

    private static final String TAG = "PerfilContratado";
    private SectionPagerAdapterContratado mSectionsPagerAdapterContratado;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_profile);
        Log.d(TAG, "onCreate: Starting");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        mSectionsPagerAdapterContratado = new SectionPagerAdapterContratado(getSupportFragmentManager());

        viewPager = (ViewPager)findViewById(R.id.container);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);



    }

    private void setupViewPager(ViewPager viewPager){

        SectionPagerAdapterContratado adapter = new SectionPagerAdapterContratado(getSupportFragmentManager());
        adapter.addFragment(new TabPerfilFragment(), "Perfil");
        adapter.addFragment(new TabAgendaFragment(), "Agenda");
        viewPager.setAdapter(adapter);

    }

    };


