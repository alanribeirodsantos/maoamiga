package com.heptron.dadm;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.heptron.dadm.DAO.FirebaseSettings;
import com.heptron.dadm.Helper.Preferences;
import com.heptron.dadm.POJO.Scheduling;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView nextServiceMenu;
    TextView schedule;
    TextView userName;
    TextView qtdSchedulings;
    TextView txtNextService;
    CardView cardNextService;
    TextView nextServiceName;
    TextView nextServiceOccup;
    TextView nextServiceDate;
    TextView nextServiceTime;
    CircleImageView nextServicePhoto;
    Preferences preferences;
    CircleImageView profilePic;
    DatabaseReference firebase;
    ValueEventListener valueEventListener;
    private ArrayList<Scheduling> schedulings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userName = (TextView) findViewById(R.id.userName);
        qtdSchedulings = (TextView) findViewById(R.id.qtdSchedulings);
        preferences = new Preferences(this);
        profilePic = (CircleImageView) findViewById(R.id.profilePicUser);
        txtNextService = (TextView) findViewById(R.id.txtNextService);
        cardNextService = (CardView) findViewById(R.id.cardNextService);
        nextServiceName = (TextView) findViewById(R.id.nextServiceName);
        nextServiceOccup = (TextView) findViewById(R.id.nextServiceOccup);
        nextServiceDate = (TextView) findViewById(R.id.nextServiceDate);
        nextServiceTime = (TextView) findViewById(R.id.nextServiceTime);
        nextServicePhoto = (CircleImageView) findViewById(R.id.nextServicePhoto);

        profilePic.setImageResource(R.drawable.man_nophoto);
        userName.setText(preferences.getName());

        preferences = new Preferences(UserProfileActivity.this);
        firebase = FirebaseSettings.getFirebase().child("schedulings");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                schedulings.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    Scheduling sch = data.getValue(Scheduling.class);
                    if(sch.getIdUser().equals(preferences.getId())){
                        schedulings.add(sch);
                    }
                }
                qtdSchedulings.setText(Integer.toString(schedulings.size()));
                if(schedulings.size() > 0){
                    Collections.sort(schedulings, new Comparator<Scheduling>() {
                        @Override
                        public int compare(Scheduling s1, Scheduling s2) {
                            //return Integer.parseInt(s1.getDay()) - Integer.parseInt(s2.getDay());
                            int comp = s1.getDay().compareTo(s2.getDay());
                            if(comp != 0){
                                return comp;
                            }
                            String[] hour1 = s1.getHour().split("h");
                            String[] hour2 = s2.getHour().split("h");
                            return Integer.parseInt(hour1[0]) - Integer.parseInt(hour2[0]);
                        }
                    });
                    txtNextService.setVisibility(View.VISIBLE);
                    cardNextService.setVisibility(View.VISIBLE);
                    nextServicePhoto.setImageResource(schedulings.get(0).getPhoto());
                    nextServiceName.setText(schedulings.get(0).getWorkerName());
                    nextServiceOccup.setText(schedulings.get(0).getOccupation());
                    nextServiceDate.setText(schedulings.get(0).getDay() + "/" + schedulings.get(0).getDate());
                    nextServiceTime.setText(schedulings.get(0).getHour());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        schedule = (TextView) findViewById(R.id.iconSchedule);
        schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserProfileActivity.this, ScheduleActivity.class);
                startActivity(intent);
            }
        });

        nextServiceMenu = (TextView) findViewById(R.id.moreMenu);
        nextServiceMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu moreMenu = new PopupMenu(UserProfileActivity.this, nextServiceMenu);
                moreMenu.getMenuInflater().inflate(R.menu.next_service_popup_menu, moreMenu.getMenu());

                moreMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getTitle().equals("Enviar mensagem")){
                            Intent intent = new Intent(UserProfileActivity.this, MessageActivity.class);
                            startActivity(intent);
                        }
                        return true;
                    }
                });

                moreMenu.show();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.action_logout){
            signOut();
        }

        return super.onOptionsItemSelected(item);
    }

    public void signOut(){
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(UserProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        firebase.addValueEventListener(valueEventListener);
    }
}
