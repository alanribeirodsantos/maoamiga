package com.heptron.dadm.DAO;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Stephane on 09/12/2017.
 */

public class FirebaseSettings {

    private static DatabaseReference firebaseReference;
    private static FirebaseAuth authentication;

    public static DatabaseReference getFirebase(){
        if(firebaseReference == null){
            firebaseReference = FirebaseDatabase.getInstance().getReference();
        }
        return firebaseReference;
    }

    public static FirebaseAuth getFirebaseAuthentication(){
        if(authentication == null){
            authentication = FirebaseAuth.getInstance();
        }
        return authentication;
    }
}
