package com.heptron.dadm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.heptron.dadm.DAO.FirebaseSettings;
import com.heptron.dadm.Helper.Base64Custom;
import com.heptron.dadm.Helper.Preferences;
import com.heptron.dadm.POJO.Scheduling;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by richa on 02/11/2017.
 */

public class TabAgendaFragment extends Fragment implements View.OnClickListener {
    public static final String RESULT = "result";
    public static final String EVENT = "event";
    private static int ADD_EVENT = 44;
    private CalendarView calendarView;
    private List<EventDay> events = new ArrayList<>();
    private Scheduling scheduling;
    private DatabaseReference firebase;
    private ValueEventListener valueEventListener;
    private ArrayList<Scheduling> schedulingsList = new ArrayList<>();
    private Preferences pr;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_agenda_fragment, container, false);
        calendarView = (CalendarView) view.findViewById(R.id.calendarView);
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();
        int days[] = intent.getIntArrayExtra("days");
        final int photo = extras.getInt("photo");
        final String occupation = extras.getString("occupation");
        final String name = extras.getString("name");


        for (int i = 0; i < days.length; i++){
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, days[i]);
            if(occupation.equals("Alvenaria")){
                events.add(new EventDay(calendar, R.drawable.alvenaria));
            }
            else if(occupation.equals("Serviços Elétricos")){
                events.add(new EventDay(calendar, R.drawable.eletricos));
            }
            else if(occupation.equals("Encanamento")){
                events.add(new EventDay(calendar, R.drawable.encanamento));
            }
            else if(occupation.equals("Faxina")){
                events.add(new EventDay(calendar, R.drawable.faxina));
            }
            else if(occupation.equals("Instalação de Gás")){
                events.add(new EventDay(calendar, R.drawable.gas));
            }
            else if(occupation.equals("Jardinagem")){
                events.add(new EventDay(calendar, R.drawable.jardinagem));
            }
            else if(occupation.equals("Pintura")){
                events.add(new EventDay(calendar, R.drawable.pintura));
            }
            else if(occupation.equals("Reparo de Móveis")){
                events.add(new EventDay(calendar, R.drawable.reparo));
            }
        }


        if(schedulingsList.size() > 0){
            pr = new Preferences(getActivity());
            for (int i = 0; i < schedulingsList.size(); i++){
                if(schedulingsList.get(i).getIdUser() == pr.getId()){
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(schedulingsList.get(i).getDay()));
                    events.add(new EventDay(calendar, R.drawable.jardinagem));
                }

            }
            calendarView.setEvents(events);
        }

        calendarView = (CalendarView) view.findViewById(R.id.calendarView);
        calendarView.setEvents(events);

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar calendar = Calendar.getInstance();
                final Preferences preferences = new Preferences(getActivity());
                final int day = eventDay.getCalendar().get(Calendar.DAY_OF_MONTH);
                final int month = eventDay.getCalendar().get(Calendar.MONTH);

                if((eventDay.getCalendar().get(Calendar.DAY_OF_MONTH) < calendar.get(Calendar.DAY_OF_MONTH)
                        && eventDay.getCalendar().get(Calendar.YEAR) <= calendar.get(Calendar.YEAR))
                        || (eventDay.getCalendar().get(Calendar.MONTH) < calendar.get(Calendar.MONTH)
                        && eventDay.getCalendar().get(Calendar.YEAR) <= calendar.get(Calendar.YEAR))){
                    Toast.makeText(getActivity(), "Ação não permitida", Toast.LENGTH_SHORT).show();

                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getLayoutInflater();
                    ViewGroup view = (ViewGroup) inflater.inflate(R.layout.content_dialog_schedule, null);
                    final Spinner spinner = (Spinner) view.findViewById(R.id.spinnerHour);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.hours_array));
                    spinner.setAdapter(adapter);
                    builder.setView(view);
                    builder.setPositiveButton("Agendar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            scheduling = new Scheduling();
                            String hour = spinner.getSelectedItem().toString();
                            String schedulingId = Base64Custom.encodeBase64(preferences.getName() + name + hour);
                            scheduling.setId(schedulingId);
                            scheduling.setWorkerName(name);
                            scheduling.setOccupation(occupation);
                            scheduling.setDay(Integer.toString(day));
                            scheduling.setDate(Integer.toString(month));
                            scheduling.setIdUser(preferences.getId());
                            scheduling.setHour(hour);
                            scheduling.setPhoto(photo);
                            addProduto(scheduling);
                        }
                    });
                    builder.show();
                    /*Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.content_dialog_schedule);
                    dialog.show();*/

                }

            }
        });

        return view;
    }

    private boolean addProduto(Scheduling scheduling){
        try{
            firebase = FirebaseSettings.getFirebase().child("schedulings");
            firebase.child(scheduling.getId()).setValue(scheduling);
            Toast.makeText(getContext(), "Serviço agendado com sucesso!", Toast.LENGTH_SHORT).show();
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(scheduling.getDay()));
            if(scheduling.getOccupation().equals("Alvenaria")){
                events.add(new EventDay(calendar, R.drawable.alvenaria));
            }
            else if(scheduling.getOccupation().equals("Serviços Elétricos")){
                events.add(new EventDay(calendar, R.drawable.eletricos));
            }
            else if(scheduling.getOccupation().equals("Encanamento")){
                events.add(new EventDay(calendar, R.drawable.encanamento));
            }
            else if(scheduling.getOccupation().equals("Faxina")){
                events.add(new EventDay(calendar, R.drawable.faxina));
            }
            else if(scheduling.getOccupation().equals("Instalação de Gás")){
                events.add(new EventDay(calendar, R.drawable.gas));
            }
            else if(scheduling.getOccupation().equals("Jardinagem")){
                events.add(new EventDay(calendar, R.drawable.jardinagem));
            }
            else if(scheduling.getOccupation().equals("Pintura")){
                events.add(new EventDay(calendar, R.drawable.pintura));
            }
            else if(scheduling.getOccupation().equals("Reparo de Móveis")){
                events.add(new EventDay(calendar, R.drawable.reparo));
            }
            calendarView.setEvents(events);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setPositiveButton("OK", null);
        builder.setView(listView);
        AlertDialog dialog = builder.create();
        dialog.show();


//        Dialog dialog = new Dialog(getActivity());
//
//        dialog.setTitle("Horarios Livres");
//        dialog.setContentView(R.layout.dialogagenda);
//
//
//        dialog.show();*/
    }


}



