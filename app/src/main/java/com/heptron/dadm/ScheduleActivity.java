package com.heptron.dadm;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.heptron.dadm.DAO.FirebaseSettings;
import com.heptron.dadm.Helper.Preferences;
import com.heptron.dadm.POJO.Scheduling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ScheduleActivity extends AppCompatActivity {
    private ArrayList<Scheduling> schedulings = new ArrayList<>();
    private ArrayList<Scheduling> sch = new ArrayList<>();
    private DatabaseReference firebase;
    private ValueEventListener valueEventListener;
    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.scheduleRecycler);
        final ScheduleAdapter adapter = new ScheduleAdapter(this, schedulings);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        preferences = new Preferences(ScheduleActivity.this);
        firebase = FirebaseSettings.getFirebase().child("schedulings");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                schedulings.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    Scheduling sch = data.getValue(Scheduling.class);
                    if(sch.getIdUser().equals(preferences.getId())){
                        schedulings.add(sch);
                        Collections.sort(schedulings, new Comparator<Scheduling>() {
                            @Override
                            public int compare(Scheduling s1, Scheduling s2) {
                                //return Integer.parseInt(s1.getDay()) - Integer.parseInt(s2.getDay());
                                int comp = s1.getDay().compareTo(s2.getDay());
                                if(comp != 0){
                                    return comp;
                                }
                                String[] hour1 = s1.getHour().split("h");
                                String[] hour2 = s2.getHour().split("h");
                                return Integer.parseInt(hour1[0]) - Integer.parseInt(hour2[0]);
                            }
                        });
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

    }

    @Override
    public void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        firebase.addValueEventListener(valueEventListener);
    }

}
