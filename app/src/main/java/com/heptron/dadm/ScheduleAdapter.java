package com.heptron.dadm;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.heptron.dadm.DAO.FirebaseSettings;
import com.heptron.dadm.POJO.Scheduling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyHolder> {

    private Context context;
    private ArrayList<Scheduling> schedulings;

    /*private int photos[];
    private String names[];
    private String occupations[];
    private String date[];
    private String time[];


    public ScheduleAdapter(Context context, int[] photos, String[] names, String[] occupations, String[] date, String[] time) {
        this.context = context;
        this.photos = photos;
        this.names = names;
        this.occupations = occupations;
        this.date = date;
        this.time = time;
    }*/

    public ScheduleAdapter(Context context, ArrayList<Scheduling> schedulings){
        this.context = context;
        this.schedulings = schedulings;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_schedule, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position){
        holder.scheduleProfile.setImageResource(schedulings.get(position).getPhoto());
        holder.scheduleName.setText(schedulings.get(position).getWorkerName());
        holder.occupation.setText(schedulings.get(position).getOccupation());
        String date = schedulings.get(position).getDay() + "/" + schedulings.get(position).getDate();
        holder.scheduleDate.setText(date);
        holder.scheduleTime.setText(schedulings.get(position).getHour());
    }

    /*@Override
    public int getItemCount(){
        return names.length;
    }*/

    @Override
    public int getItemCount(){
        return schedulings.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        CircleImageView scheduleProfile;
        TextView scheduleName;
        TextView occupation;
        TextView scheduleDate;
        TextView scheduleTime;
        View separator;

        public MyHolder(final View itemView) {
            super(itemView);
            scheduleProfile = (CircleImageView) itemView.findViewById(R.id.scheduleProfile);
            scheduleName = (TextView) itemView.findViewById(R.id.scheduleName);
            occupation = (TextView) itemView.findViewById(R.id.scheduleOccupation);
            scheduleDate = (TextView) itemView.findViewById(R.id.scheduleDate);
            scheduleTime = (TextView) itemView.findViewById(R.id.scheduleTime);
            separator = (View) itemView.findViewById(R.id.separator);
            separator.getBackground().setAlpha(128);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Context context = itemView.getContext();
                    context.startActivity(new Intent(context, WorkerProfile.class));*/
                }
            });
        }
    }
}
