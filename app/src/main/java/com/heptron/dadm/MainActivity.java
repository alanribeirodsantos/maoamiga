package com.heptron.dadm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.heptron.dadm.DAO.FirebaseSettings;
import com.heptron.dadm.Helper.Preferences;
import com.heptron.dadm.POJO.User;

public class MainActivity extends AppCompatActivity {
    private EditText edtEmail;
    private EditText edtPassword;
    private Button btnLogin;
    private TextView signUp;
    private FirebaseAuth authentication;
    private User user;
    private User currentUser;
    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authentication = FirebaseAuth.getInstance();
        preferences = new Preferences(this);
        user = new User();

        if(authentication.getCurrentUser() != null){
            ProgressDialog pg = new ProgressDialog(MainActivity.this);
            pg.setMessage("Logando como " + preferences.getName());
            pg.show();
            user.setEmail(preferences.getEmail());
            user.setPassword(preferences.getPassword());
            authLogin();
        }

        edtEmail = (EditText) findViewById(R.id.editTextUser);
        edtPassword = (EditText) findViewById(R.id.editTextPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        signUp = (TextView) findViewById(R.id.signUp);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSignUp();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edtEmail.getText().toString().equals("") && !edtPassword.getText().toString().equals("")){
                    user = new User();
                    user.setEmail(edtEmail.getText().toString());
                    user.setPassword(edtPassword.getText().toString());
                    authLogin();
                }
                else {
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    CharSequence text = "Usuário ou senha incorretos!";
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        edtEmail = (EditText) findViewById(R.id.editTextUser);
        edtPassword = (EditText) findViewById(R.id.editTextPassword);

        edtEmail.getText().clear();
        edtPassword.getText().clear();
    }

    private void authLogin(){
        authentication = FirebaseSettings.getFirebaseAuthentication();
        authentication.signInWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser firebaseUser = task.getResult().getUser();
                    DatabaseReference firebase = FirebaseSettings.getFirebase().child("users").child(firebaseUser.getUid());
                    firebase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            currentUser = dataSnapshot.getValue(User.class);
                            preferences.saveUserPreferences(currentUser.getId(), currentUser.getName(), currentUser.getEmail(), currentUser.getPassword(), currentUser.getAddress());
                            openServicesActivity();
                            Toast.makeText(MainActivity.this, "Login efetuado com sucesso!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }else{
                    Toast.makeText(MainActivity.this, "Falha no Login", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void openServicesActivity(){
        Intent intent = new Intent(this, ServicesActivity.class);
        startActivity(intent);
        //finish();
    }

    public void openSignUp(){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }
}
