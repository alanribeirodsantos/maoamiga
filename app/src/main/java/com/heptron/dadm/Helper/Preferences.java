package com.heptron.dadm.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Stephane on 09/12/2017.
 */

public class Preferences {

    private Context context;
    private SharedPreferences preferences;
    private String FILE_NAME = "FirebasePreferences.preferences";
    private int MODE = 0;
    private SharedPreferences.Editor editor;
    private final String ID_KEY = "idLoggedUser";
    private final String NAME_KEY = "nameLoggedUser";
    private final String EMAIL_KEY = "emailLoggedUser";
    private final String PASSWORD_KEY = "passwordLoggedUser";
    private final String ADDRESS_KEY = "addressLoggedUser";

    public Preferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(FILE_NAME, MODE);
        editor = preferences.edit();
    }

    public void saveUserPreferences(String id, String name, String email, String password, String address){
        editor.putString(ID_KEY, id);
        editor.putString(NAME_KEY, name);
        editor.putString(EMAIL_KEY, email);
        editor.putString(PASSWORD_KEY, password);
        editor.putString(ADDRESS_KEY, address);
        editor.commit();
    }

    public String getId(){
        return preferences.getString(ID_KEY, null);
    }

    public String getName(){
        return preferences.getString(NAME_KEY, null);
    }

    public String getEmail(){
        return preferences.getString(EMAIL_KEY, null);
    }

    public String getPassword(){ return preferences.getString(PASSWORD_KEY, null);}

    public String getAddress(){ return preferences.getString(ADDRESS_KEY, null);}

}
