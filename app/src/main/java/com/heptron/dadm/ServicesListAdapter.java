package com.heptron.dadm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by AlanR on 24/10/2017.
 */

public class ServicesListAdapter extends RecyclerView.Adapter<ServicesListAdapter.MyHolder> {

    private Context context;
    private int icones[];
    private String nomes[];
    private String qtd[];

    public ServicesListAdapter(Context context, int[] icones, String[] nomes, String[] qtd) {
        this.context = context;
        this.icones = icones;
        this.nomes = nomes;
        this.qtd = qtd;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_services_grid_layout, null);
        MyHolder myHolder = new MyHolder(layout);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.imgview.setImageResource(icones[position]);
        holder.txtService.setText(nomes[position]);
        holder.txtQtd.setText(qtd[position]);
    }

    @Override
    public int getItemCount() {
        return nomes.length;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        ImageView imgview;
        TextView txtService;
        TextView txtQtd;
        CardView cardview;
        View separator;

        public MyHolder(final View itemView) {
            super(itemView);
            imgview = (ImageView) itemView.findViewById(R.id.imageView);
            cardview = (CardView) itemView.findViewById(R.id.cardView);
            separator = (View) itemView.findViewById(R.id.separatorService);
            separator.getBackground().setAlpha(128);
            txtService = (TextView) itemView.findViewById(R.id.textViewService);
            txtQtd = (TextView) itemView.findViewById(R.id.textViewQtd);
            cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = itemView.getContext();
                    /*context.startActivity(new Intent(context, WorkersActivity.class));*/
                    if(txtService.getText() == "Alvenaria"){
                        String titulo = "Profissionais de Alvenaria";

                        int[] alvenaria = {R.drawable.alvenaria_1, R.drawable.man_nophoto, R.drawable.alvenaria_2, R.drawable.man_nophoto};

                        String[] alvenariaNomes = {"José Maria", "Roberto Martins", "Alberto Leonardo", "Marcos Rocha"};

                        String[] online = {"online", "2m", "online", "online"};

                        String[] enderecos = {"Rua Nossa Senhora Aparecida, Putiú", "Vila Belchior, Alto São Francisco", "Vila do Heitor, Campo Velho",
                                              "Rua Nossa Senhora da Conceição, Combate"};

                        String[] telefones = {"(88) 99765-7856", "(88) 99653-8901", "(88) 99654-0081", "(88) 9256-7761"};

                        int[] estrelas = {4, 3, 4, 4, 3};

                        int[][] daysBusy = new int[][]{
                                new int[] {15, 18, 19, 22, 27},
                                new int[] {16, 22},
                                new int[] {18, 20, 21, 28},
                                new int[] {20, 27, 28}
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", alvenaria);
                        extras.putStringArray("nomes", alvenariaNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else if(txtService.getText() == "Elétricos"){
                        String titulo = "Profissionais de Serviços Elétricos";

                        int[] eletricos = {R.drawable.eletricos_1, R.drawable.woman_nophoto, R.drawable.eletricos_2, R.drawable.man_nophoto,
                                R.drawable.man_nophoto};

                        String[] eletricosNomes = {"Jonas Silva", "Valéria Correa", "Joaquim Feitosa", "Leandro Arruda", "Roger Almeida"};

                        String[] online = {"5h", "online", "online", "3h", "online"};

                        String[] enderecos = {"Rua Jesus Maria e José, Campo Novo", "Beco Paralela, São João", "Travessa Estudante Antonio Brito, Centro",
                                "Rua Ibiapina, São João", "Travessa Donatila Viana, Irajá"};

                        String[] telefones = {"(88) 98854-9821", "(85) 98745-0834", "(88) 99612-8901", "(88) 99254-9856", "(88) 99964-1274"};

                        int[] estrelas = {2, 4, 5, 5, 3};

                        int[][] daysBusy = new int[][]{
                                new int[] {16, 18, 19, 20, 26},
                                new int[] {18, 28, 29},
                                new int[] {15, 17},
                                new int[] {18, 19, 28},
                                new int[] {20, 21}
                        };


                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", eletricos);
                        extras.putStringArray("nomes", eletricosNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else if(txtService.getText() == "Encanamento"){
                        String titulo = "Profissionais de Encanamento";

                        int[] encanamento = {R.drawable.encanamento_1, R.drawable.woman_nophoto, R.drawable.encanamento_2};

                        String[] encanamentoNomes = {"Carlos Oliveira", "Paula Alves", "Adriano Santos"};

                        String[] online = {"online", "2m", "15m"};

                        String[] enderecos = {"Rua Raimundo Tavares de Almeida, Jardim dos Monólitos", "Travessa Zeque Roque, Planalto Renascer",
                                "Travessa José Capistrano Filho, Curicaca"};

                        String[] telefones = {"(88) 98156-9866", "(85) 98564-8975", "(88) 99625-1234"};

                        int[] estrelas = {4, 4, 5};

                        int[][] daysBusy = new int[][]{
                                new int[] {19, 20},
                                new int[] {16, 30},
                                new int[] {17, 20, 21, 23}
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", encanamento);
                        extras.putStringArray("nomes", encanamentoNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else if(txtService.getText() == "Faxina"){
                        String titulo = "Profissionais de Faxina";

                        int[] faxina = {R.drawable.faxina_1, R.drawable.woman_nophoto, R.drawable.faxina_2, R.drawable.woman_nophoto};

                        String[] faxinaNomes = {"Dalva Soares", "Maria do Socorro", "Rogerio Alcantara", "Karla Machado"};

                        String[] online = {"online", "online", "online", "online"};

                        String[] enderecos = {"Beco 2 Estados Unidos, São João", "Rua Saturnina Silveira Lima, Alto São Francisco",
                                "Rua Adalto Lino, Alto São Francisco", "Rua 9, Nova Jerusalém"};

                        String[] telefones = {"(88) 94756-8574", "(85) 98522-9856", "(88) 95687-3467"};

                        int[] estrelas = {4, 3, 4, 3};

                        int[][] daysBusy = new int[][]{
                                new int[] {19, 20, 22},
                                new int[] {15, 16, 18},
                                new int[] {20, 26, 28},
                                new int[] {20}
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", faxina);
                        extras.putStringArray("nomes", faxinaNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else if(txtService.getText() == "Instalação de Gás"){
                        String titulo = "Profissionais de Instalação de Gás";

                        int[] gas = {R.drawable.gas_1, R.drawable.man_nophoto};

                        String[] gasNomes = {"Raquel Maia", "Jorge Macário"};

                        String[] online = {"25m", "1h"};

                        String[] enderecos = {"Rua Pascoal Crispino, Centro ", "1º Travessa Equador, São João"};

                        String[] telefones = {"(88) 97645-8967", "(88) 94563-3321"};

                        int[] estrelas = {4, 4};

                        int[][] daysBusy = new int[][]{
                                new int[] {15, 27},
                                new int[] {16},
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", gas);
                        extras.putStringArray("nomes", gasNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else if(txtService.getText() == "Jardinagem"){
                        String titulo = "Profissionais de Jardinagem";

                        int[] jardinagem = {R.drawable.jardinagem_1, R.drawable.woman_nophoto, R.drawable.jardinagem_2};

                        String[] jardinagemNomes = {"Ronaldo Farias", "Karina Almeida", "Isis Teixeira"};

                        String[] online = {"10m", "5h", "online"};

                        String[] enderecos = {"Travessa Manoel Monte, Centro", "Travessa Robério Damasceno, Planalto Universitário",
                                "Travessa Adalto Lino, Alto São Francisco"};

                        String[] telefones = {"(88) 99971-0956", "(88) 99675-8975", "(88) 98165-0008"};

                        int[] estrelas = {4, 4, 5};

                        int[][] daysBusy = new int[][]{
                                new int[] {15},
                                new int[] {27, 29},
                                new int[] {18, 20}
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", jardinagem);
                        extras.putStringArray("nomes", jardinagemNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else if(txtService.getText() == "Pintura"){
                        String titulo = "Profissionais de Pintura";

                        int[] pintura = {R.drawable.pintura_1, R.drawable.pintura_2, R.drawable.pintura_3, R.drawable.man_nophoto};

                        String[] pinturaNomes = {"Rosa dos Santos", "Roberto Carlos", "Karina Leitte", "Jorge Silva"};

                        String[] online = {"online", "2m", "online", "online"};

                        String[] enderecos = {"Beco 2 Francisco Batista de Queiroz, Alto Boa Vista", "Rua São Francisco, Triângulo",
                                "Travessa Chagas Holanda, Centro", "Travessa José de Alencar, Baviera"};

                        String[] telefones = {"(88) 99964-9860", "(88) 99253-8745", "(88) 99624-6621", "(88) 99964-0956"};

                        int[] estrelas = {3, 4, 5, 5};

                        int[][] daysBusy = new int[][]{
                                new int[] {18, 19, 29},
                                new int[] {15, 17, 28},
                                new int[] {22, 23, 27},
                                new int[] {20}
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", pintura);
                        extras.putStringArray("nomes", pinturaNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                    else{
                        String titulo = "Profissionais de Reparo de Móveis";

                        int[] reparo = {R.drawable.reparo_1, R.drawable.woman_nophoto};

                        String[] reparoNomes = {"Junior Carlos", "Ana Silva"};

                        String[] online = {"online", "online"};

                        String[] enderecos = {"Travessa Amauri Cidade, Centro", "Rua Régis Brasil, Carrascal"};

                        String[] telefones = {"(88) 98156-8567", "(85) 99653-8574"};

                        int[] estrelas = {4, 4};

                        int[][] daysBusy = new int[][]{
                                new int[] {15, 27},
                                new int[] {16, 23, 29}
                        };

                        Intent intent = new Intent(context, WorkersActivity.class);
                        Bundle extras = new Bundle();

                        extras.putString("titulo", titulo);
                        extras.putIntArray("fotos", reparo);
                        extras.putStringArray("nomes", reparoNomes);
                        extras.putStringArray("online", online);
                        extras.putStringArray("enderecos", enderecos);
                        extras.putStringArray("telefones", telefones);
                        extras.putIntArray("estrelas", estrelas);
                        extras.putSerializable("days", daysBusy);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    }
                }
            });
        }

    }
}
