package com.heptron.dadm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by AlanR on 02/11/2017.
 */

public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.MyHolder> {

    private Context context;
    private int photos[];
    private String names[];
    private String occupations[];
    private String addresses[];
    private String phones[];
    private int evaluations[];
    private int daysBusy[][];

    public TrendingAdapter(Context context, int[] photos, String[] names, String[] occupations, String[] addresses, String[] phones, int[] evaluations, int[][] daysBusy) {
        this.context = context;
        this.photos = photos;
        this.names = names;
        this.occupations = occupations;
        this.addresses = addresses;
        this.phones = phones;
        this.evaluations = evaluations;
        this.daysBusy = daysBusy;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_trending_searches, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position){
        final int pos = position;
        holder.profilePic.setImageResource(photos[position]);
        holder.nameTrending.setText(names[position]);
        holder.occupation.setText(occupations[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, WorkerProfile.class);
                Bundle extras = new Bundle();
                intent.putExtra("photo", photos[pos]);
                intent.putExtra("name", names[pos]);
                intent.putExtra("address", addresses[pos]);
                intent.putExtra("phone", phones[pos]);
                intent.putExtra("evaluation", evaluations[pos]);
                intent.putExtra("occupation", occupations[pos]);
                intent.putExtra("days", daysBusy[pos]);

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount(){
        return names.length;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        CircleImageView profilePic;
        TextView nameTrending;
        TextView occupation;
        View separator;

        public MyHolder(View itemView) {
            super(itemView);
            profilePic = (CircleImageView) itemView.findViewById(R.id.profilePicTrending);
            nameTrending = (TextView) itemView.findViewById(R.id.nameTrending);
            occupation = (TextView) itemView.findViewById(R.id.occupationTrending);
            separator = (View) itemView.findViewById(R.id.separator);
            separator.getBackground().setAlpha(128);

        }
    }
}
