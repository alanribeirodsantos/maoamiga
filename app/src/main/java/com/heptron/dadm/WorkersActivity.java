package com.heptron.dadm;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.heptron.dadm.Helper.Preferences;


public class WorkersActivity extends AppCompatActivity {

    int photos[];
    String names[];
    String nms[];
    String online[];
    String addresses[];
    String ads[];
    String phones[];
    int evaluation[];
    int daysBusy[][];
    Preferences preferences;
    String userAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMaps);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        preferences = new Preferences(WorkersActivity.this);
        ads = extras.getStringArray("enderecos");
        nms = extras.getStringArray("nomes");
        userAddress = preferences.getAddress();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMap = new Intent(WorkersActivity.this, MapsActivity.class);
                Bundle extras = new Bundle();
                extras.putStringArray("ads", ads);
                extras.putStringArray("nms", nms);
                extras.putString("userAd", userAddress);
                intentMap.putExtras(extras);
                startActivity(intentMap);
            }
        });

        String titulo = extras.getString("titulo");
        getSupportActionBar().setTitle(titulo);
        photos = extras.getIntArray("fotos");
        names = extras.getStringArray("nomes");
        online = extras.getStringArray("online");
        addresses = extras.getStringArray("enderecos");
        phones = extras.getStringArray("telefones");
        evaluation = extras.getIntArray("estrelas");
        daysBusy = (int[][]) extras.getSerializable("days");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.workersRecycler);
        WorkersAdapter adapter = new WorkersAdapter(this, photos, names, online, addresses, phones, evaluation, titulo, daysBusy);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

    }
}
