package com.heptron.dadm;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by richa on 02/11/2017.
 */

public class TabPerfilFragment extends Fragment {
    private static final String TAG = "Perfil";
    ImageView profile;
    TextView wName;
    TextView wOccupation;
    TextView wAddress;
    TextView wPhone;
    ImageView f1, f2, f3, f4, f5, e1, e2, e3, e4, e5;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_perfil_fragment, container, false);
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();
        int photo = extras.getInt("photo");
        String name = extras.getString("name");
        String address = extras.getString("address");
        String phone = extras.getString("phone");
        String occupation = extras.getString("occupation");
        int evaluation = extras.getInt("evaluation");

        profile = (ImageView) view.findViewById(R.id.imageViewProfile);
        profile.setImageResource(photo);
        wName = (TextView) view.findViewById(R.id.workerName);
        wName.setText(name);
        wOccupation = (TextView) view.findViewById(R.id.workerOccupation);
        wOccupation.setText(occupation);
        wAddress = (TextView) view.findViewById(R.id.workerAddress);
        wAddress.setText(address);
        wPhone = (TextView) view.findViewById(R.id.workerPhone);
        wPhone.setText(phone);

        f1 = (ImageView) view.findViewById(R.id.f1);
        f2 = (ImageView) view.findViewById(R.id.f2);
        f3 = (ImageView) view.findViewById(R.id.f3);
        f4 = (ImageView) view.findViewById(R.id.f4);
        f5 = (ImageView) view.findViewById(R.id.f5);
        e1 = (ImageView) view.findViewById(R.id.e1);
        e2 = (ImageView) view.findViewById(R.id.e2);
        e3 = (ImageView) view.findViewById(R.id.e3);
        e4 = (ImageView) view.findViewById(R.id.e4);
        e5 = (ImageView) view.findViewById(R.id.e5);

        if(evaluation == 1){
            f1.setVisibility(View.VISIBLE);
            e1.setVisibility(View.VISIBLE);
            e2.setVisibility(View.VISIBLE);
            e3.setVisibility(View.VISIBLE);
            e4.setVisibility(View.VISIBLE);
        }
        else if(evaluation == 2){
            f1.setVisibility(View.VISIBLE);
            f2.setVisibility(View.VISIBLE);
            e1.setVisibility(View.VISIBLE);
            e2.setVisibility(View.VISIBLE);
            e3.setVisibility(View.VISIBLE);
        }
        else if(evaluation == 3){
            f1.setVisibility(View.VISIBLE);
            f2.setVisibility(View.VISIBLE);
            f3.setVisibility(View.VISIBLE);
            e1.setVisibility(View.VISIBLE);
            e2.setVisibility(View.VISIBLE);
        }
        else if(evaluation == 4){
            f1.setVisibility(View.VISIBLE);
            f2.setVisibility(View.VISIBLE);
            f3.setVisibility(View.VISIBLE);
            f4.setVisibility(View.VISIBLE);
            e1.setVisibility(View.VISIBLE);
        }
        else if(evaluation == 5){
            f1.setVisibility(View.VISIBLE);
            f2.setVisibility(View.VISIBLE);
            f3.setVisibility(View.VISIBLE);
            f4.setVisibility(View.VISIBLE);
            f5.setVisibility(View.VISIBLE);
        }

        return view;


    }
}
