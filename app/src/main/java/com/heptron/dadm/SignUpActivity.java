package com.heptron.dadm;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.heptron.dadm.DAO.FirebaseSettings;
import com.heptron.dadm.Helper.Base64Custom;
import com.heptron.dadm.Helper.Preferences;
import com.heptron.dadm.POJO.User;

public class SignUpActivity extends AppCompatActivity {

    private EditText signUpName;
    private EditText signUpEmail;
    private EditText signUpPassword;
    private EditText signUpConfirmPassword;
    private EditText signUpAddress;
    private Button btnSignUp;
    private User user;
    private FirebaseAuth authentication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        signUpName = (EditText) findViewById(R.id.signUpName);
        signUpEmail = (EditText) findViewById(R.id.signUpEmail);
        signUpPassword = (EditText) findViewById(R.id.signUpPassword);
        signUpConfirmPassword = (EditText) findViewById(R.id.signUpConfirmPassword);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        signUpAddress = (EditText) findViewById(R.id.signUpAddress);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(signUpPassword.getText().toString().equals(signUpConfirmPassword.getText().toString())){
                    user = new User();
                    user.setName(signUpName.getText().toString());
                    user.setEmail(signUpEmail.getText().toString());
                    user.setPassword(signUpPassword.getText().toString());
                    user.setAddress(signUpAddress.getText().toString());
                    addUser();
                }else{
                    Toast.makeText(SignUpActivity.this, "As senhas não correspondem!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void addUser(){
        authentication = FirebaseSettings.getFirebaseAuthentication();
        authentication.createUserWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(SignUpActivity.this, "Usuário cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
                    String idUser = Base64Custom.encodeBase64(user.getEmail());
                    FirebaseUser firebaseUser = task.getResult().getUser();
                    user.setId(firebaseUser.getUid());
                    user.save();

                    Preferences preferences = new Preferences(SignUpActivity.this);
                    preferences.saveUserPreferences(firebaseUser.getUid(), user.getName(), user.getEmail(), user.getPassword(), user.getAddress());
                    openLoginActivity();
                }else{
                    String exceptionError = "";
                    try{
                        throw task.getException();
                    }catch(FirebaseAuthWeakPasswordException e){
                        exceptionError = "Digite uma senha mais forte, contendo no mínimo 8 caracteres entre letras e números";
                    }catch(FirebaseAuthInvalidCredentialsException e){
                        exceptionError = "O email digitado é inválido! Por favor, digite um novo e-mail";
                    }catch(FirebaseAuthUserCollisionException e){
                        exceptionError = "Este e-mail já está cadastrado!";
                    }catch(Exception e){
                        exceptionError = "Erro ao efetuar o cadastro!";
                        e.printStackTrace();
                    }
                    Toast.makeText(SignUpActivity.this, "Erro: " + exceptionError, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void openLoginActivity(){
        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
