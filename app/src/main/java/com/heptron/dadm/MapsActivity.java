package com.heptron.dadm;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.heptron.dadm.Helper.Preferences;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String userLocation;
    private String addresses[];
    private String names[];
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        preferences = new Preferences(this);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        userLocation = extras.getString("userAd");
        addresses = extras.getStringArray("ads");
        names = extras.getStringArray("nms");

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try{
            loadLocations();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void goToLocationZoom(double lat, double lng, float zoom, String name, String address){
        LatLng location = new LatLng(lat, lng);
        CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(location, zoom);
        mMap.moveCamera(camera);
        if(name.equals(preferences.getName())){
            mMap.addMarker(new MarkerOptions().position(location)
                    .title("Você")
                    .snippet(address)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        }
        else{
            mMap.addMarker(new MarkerOptions().position(location)
                    .title(name)
                    .snippet(address)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        }
    }

    private void loadLocations() throws IOException{
        Geocoder geocoder = new Geocoder(this);
        for(int i = 0; i < addresses.length; i++){
            List<Address> list = geocoder.getFromLocationName(addresses[i] + ", Quixadá, Ceará", 1);
            Address address = list.get(0);
            String local = address.getLocality();

            double lat = address.getLatitude();
            double lng = address.getLongitude();
            goToLocationZoom(lat, lng, 14, names[i], addresses[i]);
        }
        List<Address> userLoc = geocoder.getFromLocationName(userLocation + ", Quixadá, Ceará", 1);
        Address ads = userLoc.get(0);

        double latUser = ads.getLatitude();
        double lngUser = ads.getLongitude();
        goToLocationZoom(latUser, lngUser, 14, preferences.getName(), userLocation);
    }
}
