package com.heptron.dadm.POJO;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.heptron.dadm.DAO.FirebaseSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephane on 09/12/2017.
 */

public class User {

    private String id;
    private String email;
    private String password;
    private String name;
    private String address;
    private ArrayList<String> schedulingList;

    public User() {
    }

    public void save(){
        DatabaseReference referenceFirebase = FirebaseSettings.getFirebase();
        referenceFirebase.child("users").child(String.valueOf(getId())).setValue(this);
    }

    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> hashMapUser = new HashMap<>();
        hashMapUser.put("id", getId());
        hashMapUser.put("name", getName());
        hashMapUser.put("email", getEmail());
        hashMapUser.put("password", getPassword());
        hashMapUser.put("address", getAddress());
        hashMapUser.put("schedulings", getSchedulingList());

        return hashMapUser;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getSchedulingList() {
        return schedulingList;
    }

    public void setSchedulingList(ArrayList<String> schedulingList) {
        this.schedulingList = schedulingList;
    }
}
