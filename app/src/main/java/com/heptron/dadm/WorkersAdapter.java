package com.heptron.dadm;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class WorkersAdapter extends RecyclerView.Adapter<WorkersAdapter.MyHolder> {

    private Context context;
    private int photos[];
    private String names[];
    private String online[];
    private String addresses[];
    private String phones[];
    private int evaluation[];
    private String titulo;
    private int daysBusy[][];

    public WorkersAdapter(Context context, int[] photos, String[] names, String[] online, String[] addresses, String[] phones, int[] evaluation, String titulo, int[][] daysBusy) {
        this.context = context;
        this.photos = photos;
        this.names = names;
        this.online = online;
        this.addresses = addresses;
        this.phones = phones;
        this.evaluation = evaluation;
        this.titulo = titulo;
        this.daysBusy = daysBusy;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_workers, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position){
        final int pos = position;
        holder.profilePicWorker.setImageResource(photos[position]);
        holder.nameWorker.setText(names[position]);
        if(online[position].equals("online")){
            holder.online.setBackgroundResource(R.drawable.online);
            holder.online.setVisibility(View.VISIBLE);
        }
        else {
            holder.onlineMin.setText(online[position]);
            holder.onlineMin.setVisibility(View.VISIBLE);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, WorkerProfile.class);
                intent.putExtra("photo", photos[pos]);
                intent.putExtra("name", names[pos]);
                intent.putExtra("address", addresses[pos]);
                intent.putExtra("phone", phones[pos]);
                intent.putExtra("evaluation", evaluation[pos]);
                intent.putExtra("days", daysBusy[pos]);
                intent.putExtra("titulo", titulo);
                if(titulo.equals("Profissionais de Alvenaria")){
                    String occupation = "Alvenaria";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Serviços Elétricos")){
                    String occupation = "Serviços Elétricos";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Encanamento")){
                    String occupation = "Encanamento";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Faxina")){
                    String occupation = "Faxina";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Instalação de Gás")){
                    String occupation = "Instalação de Gás";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Jardinagem")){
                    String occupation = "Jardinagem";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Pintura")){
                    String occupation = "Pintura";
                    intent.putExtra("occupation", occupation);
                }
                else if(titulo.equals("Profissionais de Reparo de Móveis")){
                    String occupation = "Reparo de Móveis";
                    intent.putExtra("occupation", occupation);
                }
                context.startActivity(intent);
                //context.startActivity(new Intent(context, WorkerProfile.class));
            }
        });
    }

    @Override
    public int getItemCount(){
        return names.length;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        CircleImageView profilePicWorker;
        TextView nameWorker;
        TextView online;
        TextView onlineMin;
        View separator;

        public MyHolder(final View itemView) {
            super(itemView);
            profilePicWorker = (CircleImageView) itemView.findViewById(R.id.profilePicWorker);
            nameWorker = (TextView) itemView.findViewById(R.id.nameWorker);
            online = (TextView) itemView.findViewById(R.id.online);
            onlineMin = (TextView) itemView.findViewById(R.id.onlineMin);
            separator = (View) itemView.findViewById(R.id.separator);
            separator.getBackground().setAlpha(128);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = itemView.getContext();
                    int position = getAdapterPosition();
                    //Intent intent = new Intent(context, WorkerProfile.class);
                    //intent.putExtra("photo", );
                    context.startActivity(new Intent(context, WorkerProfile.class));
                }
            });*/
        }
    }
}
