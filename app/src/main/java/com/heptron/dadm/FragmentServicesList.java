package com.heptron.dadm;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by AlanR on 22/10/2017.
 */

public class FragmentServicesList extends Fragment {

    RecyclerView recyclerView;
    GridLayoutManager layoutManager;

    private int iconesId[] = {R.drawable.alvenaria, R.drawable.eletricos, R.drawable.encanamento, R.drawable.faxina,
                              R.drawable.gas, R.drawable.jardinagem, R.drawable.pintura, R.drawable.reparo};

    private String listaNomes[] = {"Alvenaria", "Elétricos", "Encanamento", "Faxina", "Instalação de Gás", "Jardinagem",
                                    "Pintura", "Reparo de Móveis"};

    private String quantidade[] = {"4 pessoas", "5 pessoas", "3 pessoas", "4 pessoas", "2 pessoas", "3 pessoas", "4 pessoas",
                                   "2 pessoas"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_services, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        ServicesListAdapter servicesListAdapter = new ServicesListAdapter(getActivity(), iconesId, listaNomes, quantidade);
        recyclerView.setAdapter(servicesListAdapter);
        return view;
    }
}
