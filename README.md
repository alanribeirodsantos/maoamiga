// Comandos básicos GIT

git init : Iniciar o repositorio já criado

git branch <nome> : Viaja pelas branchs.

git status : Mostrar os artefatos que foram alterados, adicionados, deletados, qualquer que seja a ação feita na pasta do repo.

git add . : Adiciona os artefatos para commitar.

git push <nome da branch> : Envia os arquivos para a branch atual.

git push origin master : Envia os arquivos comitados para a master

git checkout master : Sincroniza o repositorio com a master.